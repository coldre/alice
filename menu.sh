
# menu
showMenu() {
	talk "$ASKS_WHAT_CAN_I_DO" "onbackground"
	select yn in "update os" "files changed" "files review" "take a break"; do
		case $yn in
			"update os") updateOs; break ;;
			"files changed") filesChanged; break ;;
			"files review") filesReview; break ;;
			"take a break") sayGoodbye; break ;;
		esac
	done
}
