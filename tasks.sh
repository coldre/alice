#!/usr/bin/env bash

# tasks
case "$1" in
	"")
		setupAlice
		showHeader
		sayHi
		showMenu
	;;
	"say")
		case "$2" in
			"hi") sayHi ;;
			"goodbye") sayGoodbye ;;
		esac
	;;
	"commit")
		filesCommit "$2"
	;;
	"show")
		case "$2" in
			"files")
				case "$3" in
					"changed") filesChanged ;;
				esac
			;;
			"projects") showProjects ;;
		esac
	;;
	"review")
		case "$2" in
			"files") filesReview ;;
		esac
	;;
	"go")
		case "$2" in
			"to")
				case "$3" in
					"code") switchToAliceFolder exit 0 ;;
				esac
			;;
		esac
	;;
	"install")
		install
	;;
	"setup")
		case "$2" in
			"") setup ;;
			"publickey") setupPublickey ;;
			"marker") setupMarker ;;
			"zfproject") setupZfProject ;;
		esac
	;;
	"open")
		case "$2" in
			"browser") openBrowser ;;
			"typora") openTypora ;;
		esac
	;;
	"update")
		case "$2" in
			"") update ;;
			"os") updateOs ;;
			"projects") updateProjects ;;
		esac
	;;
	"start")
		case "$2" in
			"docker") startDocker ;;
		esac
	;;
	"build")
		case "$2" in
			"docker") buildDocker ;;
		esac
	;;
	"stop")
		case "$2" in
			"docker") stopDocker ;;
		esac
	;;
	"test")
		test
	;;
esac
