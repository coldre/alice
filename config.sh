
# full config
# user preferences
TZ="America/Sao_Paulo" # you can change this, set your timezone here
DEFAULT_SAUDATION="Sir" # here you can set your name
DEFAULT_MAIL="" # then, set your e-mail for Git use
AUTO_COMMIT_MESSAGE="refactor" # ok, you can change auto commit default message
AUDIO_VOLUME="ON" # and enable/disable audio
SHOW_WEATHER="ON"

# interface default colors
RED='\033[00;31m'
GREEN='\033[00;32m'
BLUE='\033[00;34m'
CYAN='\033[00;36m'
YELLOW='\033[00;33m'
WHITE='\033[01;37m'
RESET='\033[0m'

# commom paths
RSA_FILE=$HOME"/.ssh/gitlab_com_rsa"
SSH_CONFIG_FILE=$HOME"/.ssh/config"
FIND_DOCKER_COMPOSE=`find . -type f \( -name docker-compose.yml \) 2>/dev/null`
FIND_USER_COMMANDS=`find ~/.local/ -type f \( -name user_commands.txt \) 2>/dev/null`

# urls
COMPOSER_URL="https://raw.githubusercontent.com/composer/getcomposer.org/76a7060ccb93902cd7576b67264ad91c8a2700e2/web/installer"